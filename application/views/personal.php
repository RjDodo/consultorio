<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Registro</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
  
  <!-- Bootstrap core CSS -->
  </head>
  <body>

<br>
<div class="container">
      <div class="row">
          <div class="col-md-6" style="color:#fff">  

                  <ul class="nav nav-pills">
                    <li class="nav-item">
                      <a class="nav-link active " href="<?php echo base_url(); ?>index.php/inicio/registro">Inicio</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar Datos</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/ver_historial_todos">Mostrar Historias Médicas</a>
                    </li>
                  </ul>                  
          </div>
          <div class="col-md-6">  
              <img src="<?php echo base_url(); ?>img/img1_consulta.jpg" width="83%" alt="">
          </div>
     </div>
</div>
<br><br><br>

    <div class="form-group">
       <h1 align="center">CORPORATION MEDICAL LIFE</h1><br><br>
       <h3 align="center">Registro Personal de Trabajo</h3>
    </div>

  
      <div class="container">
           <?php echo form_open("inicio/registrar_personal") ?>
       <div class="row">
        <?php 
          if(isset($message)){          
          echo'<p>'.$message.'</p>';
        }
        ?>
         <div class="col-md-4">
              <div class="form-group">
                  <input class="form-control" type="text" name="first_name" placeholder="Nombre" value="<?php echo set_value('first_name')?>"><br><br>
              </div>
              <div class="form-group">
                 <input class="form-control" type="text" name="last_name" placeholder="Apellido" value="<?php echo set_value('last_name')?>"><br><br>
              </div>  
              <div class="form-group">
                  <input class="form-control" type="text" name="email" placeholder="Correo" value="<?php echo set_value('email')?>"><br><br>
              </div>
              <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="contraseña" value="<?php echo set_value('password')?>"><br><br>
              </div>

              <div class="form-group">
                <input class="form-control" type="password" name="conf_password" placeholder="confirmar contraseña" value="<?php echo set_value('conf_password')?>"><br><br>
              </div>

          </div>


          <div class="col-md-8">
            <br><br><br><br>
                <div class="form-group">
                 <textarea class="form-control" name="address" id="address" cols="30" rows="10" placeholder="Direcciòn de domicilio"><?php echo set_value ('address'); ?></textarea><br><br><br>
              </div>


            </div><!-- Fin de columna y fila-->
       </div>

                
         </div>
           <div class="form-group" align="center">
              <button class="btn btn-primary" type="submit" name="submit">Registrar Datos Personal</button>
           </div> 
        <?php echo form_close();?>
      </div> 
  
  </body>
</html>