<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Mostrar datos</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
</head>
<body>
	<nav>
		<ul>
			<li>
				<a href="<?php echo base_url(); ?>index.php/inicio/registro">Volver a registro</a><br>
				<a href="<?php echo base_url(); ?>inicio/editar">Editar</a><br>
				<a href="<?php echo base_url(); ?>inicio/eliminar">Eliminar</a><br>
				<a href="<?php echo base_url(); ?>inicio/buscar">Buscar</a><br>
				<a href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar datos</a>
			</li>
		</ul>
	</nav>

	<h1 align="center">Registro por id de usuario</h1>
	<div class="container">
		<div class="row">
			<div class="col">
				<table class="table table-bordered" align="center">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Edad</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($userForId as $user){?>
						 <tr>
						 	<td><?php echo $user->id_usuario; ?></td>
						 	<td><?php echo $user->first_name; ?></td>
						 	<td><?php echo $user->last_name; ?></td>
						 	<td><?php echo $user->email; ?></td>
						 	<td><?php echo $user->age; ?></td>
						 	<td><a href=" <?php echo base_url()?>index.php/inicio/editar/<?=$user->id_usuario?>">Editar</a>  |
						 	<a href=" <?php echo base_url()?>/inicio/eliminar"<?php $user->id_usuario ?>>Eliminar</a></td>
						 </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div><br><br><br><br><br>
	<div>
		<h1 align="center"><b>Total registro en base de datos:</b></h1>
		<strong><h2 align="center"><?php echo $total ?></h2></strong>
	</div>
	
</body>
</html>