<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Mostrar datos</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

</head>
<body>
<br>
<div class="container">
      <div class="row">
          <div class="col-md-6" style="color:#fff">  

                  <ul class="nav nav-pills">
                    <li class="nav-item">
                      <a class="nav-link active " href="<?php echo base_url(); ?>index.php/inicio/registro">Inicio</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar Datos</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/ver_historial_todos">Mostrar Historias Médicas</a>
                    </li>
                  </ul>                  
          </div>
          <div class="col-md-6">  
              <img src="<?php echo base_url(); ?>img/img1_consulta.jpg" width="83%" alt="">
          </div>
     </div>
</div>
<br><br><br>
	<?php foreach ($usuario as $user){?>
		<div class="container">
		 	<div class="row" style="background:gray" >
		 		<div class="col-md-6" align="center">
		 			Nombre:  <?php echo $user->first_name; ?><br>
		 			Apellido: <?php echo $user->last_name; ?><br>
		 			Correo:  <?php echo $user->email; ?><br>
		 			Edad:  <?php echo $user->age; ?><br><br><br>
		 		</div>
		 		<div class="col-md-6">
		 			Sexo:<?php echo $user->sex; ?><br>
		 			Direccíon:<?php echo $user->address; ?><br>
		 			Tipo de sangre:<?php echo $user->blood_type; ?><br>
		 			Fecha de regitro:<?php echo $user->registration_date; ?><br>
		 		</div>
		 	</div>
		</div> 	
			
	<?php } ?>

	<h1 align="center">HISTORIAL MÉDICO </h1>
	<div class="container">
		<div class="row">
			<div class="col-md-12" align="center">
				<?php echo form_open("inicio/buscar"); ?>
					<input type="text" name="email" id="email">
					<input class="btn btn-primary" type="submit" name="buscar" id="buscar" value="Buscar">
				<?php echo form_close(); ?>
				<br/>
				<a class="btn btn-success" href="<?php echo base_url(); ?>index.php/inicio/llenar_nueva_historia/<?php $user->id_usuario; ?>"><b>Crear nueva historia</b></a>
				<br/><br>	
			</div>
				
        </div>
        <div class="row">
        	<div class="col-md-12" align="center">
        		<table  class ="table table-striped table-hover table table-sm">
					<thead = class ="table-primary">
						<tr>
							<th>Id</th>
							<th>id_usuario</th>
							<th>sintomas</th>
							<th>Tipo de seguro</th>
							<th>Fecha de registro</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($usuario as $user){?>
						 <tr>
						 	<td><?php echo $user->id_historial;?></td>
						 	<td><?php echo $user->id_usuario; ?></td>
						 	<td><?php echo $user->sintomas; ?></td>
						 	<td><?php echo $user->tipo_seguro; ?></td>
						 	<td><?php echo $user->fecha_ingreso; ?></td>
						 	<td></td>
						 </tr>
						<?php } ?>
					</tbody>
				</table>
        	</div>
        </div>
				
			
		
	</div>
</body>
</html>