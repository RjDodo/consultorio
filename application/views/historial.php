<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Historial</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>
<body>
	<nav>
	    <ul>
	      <li>
	        <a href="<?php echo base_url(); ?>index.php/inicio/registro">Registrar</a><br>
	        <a href="<?php echo base_url(); ?>inicio/buscar">Buscar</a><br>
	        <a href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar datos</a>
	      </li>
	    </ul>
  </nav>
    <h1 align="center">HISTORIAL MÉDICO</h1>
    <br><br><br>
    
    <?php  echo validation_errors(); ?>
    <div class="container">
      <?php echo form_open("inicio/guardar_historia") ?>
      <div class="row">
        <div class="col-md-4">

          <div class="form-group">
            <label for="">Id usuario</label>
              <input type="text" name="id_usuario" id="id_usuario" value="<?=$id_usuario?> ">
          </div>

          <div class="form-group">
             <textarea name="sintomas" id="sintomas" cols="30" rows="10" placeholder="Sintomas"><?php echo set_value('sintomas')?></textarea><br><br>
          </div>
          <!-- -->
          <div class="form-group">
            <label for="">¿Posee poliza de seguro médico?</label>
            <select name="seguro" id="seguro">
              <option value="si">SI</option>
              <option value="no">NO</option>
            </select><br><br>
          </div>

          <div class="form-group">
            <label for="">Nombre del seguro:</label>
            <input type="text" name="tipo_seguro" id="tipo_seguro" placeholder="Nombre del seguro" value="<?php set_value('tipo_seguro')?>"><br><br>
          </div>

          <div class="form-group">
            <label for="">Fecha de ingreso:</label>
            <input type="date" name="fecha_ingreso" placeholder="fecha de ingreso" value="<?php echo set_value('fecha_ingreso')?>"><br><br>
          </div>

          <div class="form-group">
            <button class="btn btn-primary" type="submit" name="submit">Registrar historia</button>
          </div>

        </div>
      </div>
    	      
       <?php echo form_close();?>
    </div>
</body>
</html>