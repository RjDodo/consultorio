<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Registro</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles.css">
  
  <!-- Bootstrap core CSS -->
  </head>
  <body>
    <!-- sesion -->
    <h1> Bienvenido/a <?php echo $first_name ?> </h1>
    <p>
       <a href="<?php echo base_url() ?>inicio/cerrar_sesion"> Cerrar sesión </a>
    </p>
<br>
<div class="container">
      <div class="row">
          <div class="col-md-6" style="color:#fff">  

                  <ul class="nav nav-pills">
                    <li class="nav-item">
                      <a class="nav-link active " href="<?php echo base_url(); ?>index.php/inicio/registro">Inicio</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar Datos</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/ver_historial_todos">Mostrar Historias Médicas</a>
                    </li>
                  </ul>                  
          </div>
          <div class="col-md-6">  
              <img src="<?php echo base_url(); ?>img/img1_consulta.jpg" width="83%" alt="">
          </div>
     </div>
</div>
<br><br><br>

    <div class="form-group">
       <h1 align="center">BIENVENIDO REGISTRATE</h1>
    </div>

    <?php 
      if(isset($message)){          
          echo'<p>'.$message.'</p>';
        }
          echo validation_errors();?>
      <div class="container">
           <?php echo form_open("inicio/save") ?>
       <div class="row">
         <div class="col-md-4">
              <div class="form-group">
                  <input class="form-control" type="text" name="first_name" placeholder="Nombre" value="<?php echo set_value('first_name')?>"><br><br>
              </div>
              <div class="form-group">
                 <input class="form-control" type="text" name="last_name" placeholder="Apellido" value="<?php echo set_value('last_name')?>"><br><br>
              </div>  
              <div class="form-group">
                  <input class="form-control" type="text" name="email" placeholder="Correo" value="<?php echo set_value('email')?>"><br><br>
              </div>
              <div class="form-group">
                <input class="form-control" type="text" name="age" placeholder="Edad" value="<?php echo set_value('age')?>">
              </div>
              
              <div class="form-group">
                  <label for="sex">Sexo</label>
                  <select class="form-control" name="sex" id="sex">
                    <option value="M">Masculino</option>
                    <option value="F">Femenino</option>
                  </select><br><br>
              </div>
              
         </div>

             <div class="col-md-8">

                <div class="form-group">
                 <textarea class="form-control" name="address" id="address" cols="30" rows="10" placeholder="Direcciòn de domicilio"><?php echo set_value ('address'); ?></textarea><br><br>
                </div>


               <div class="form-group">
                  <label for="blood_type">Tipo de sangre</label>
                  <select class="form-control" name="blood_type" >
                    <option value="O POSITIVO">O POSITIVO</option>
                    <option value="O NEGATIVO">O NEGATIVO</option>
                    <option value="A POSITIVO">A POSITIVO</option>
                    <option value="A NEGATIVO">A NEGATIVO</option>
                    <option value="B POSITIVO">B POSITIVO</option>
                    <option value="B NEGATIVO">B NEGATIVO</option>
                    <option value="AB POSITIVO">AB POSITIVO</option>
                    <option value="AB NEGATIVO">AB NEGATIVO</option>
                 </select><br> <br>
              </div>

       </div><!-- Fin de columna y fila-->
             
              <div class="row"><!-- Inicio  de segunda fila -->
                
                <div class="col-md-8">
                  <div class="form-group">
                  <textarea class="form-control" name="allergies"   id="allergies" cols="100" rows="10" placeholder="Llenar si posee algùn cuadro alergico"><?php echo set_value('allergies'); ?></textarea><br><br>
                  </div> 
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">Fecha registro:</label>
                    <input class="form-control" type="date" name="registration_date" placeholder="fecha de registro" value="<?php echo set_value('registration_date')?>"><br><br>
                  </div>

                 <div class="form-group">
                   <label for="">Fecha modificacion:</label>
                   <input class="form-control" type="date" name="modification_date" placeholder="fecha de registro" value="<?php echo set_value('modification_date')?>"><br><br>
                 </div>

                </div>

              </div>
            

             
         </div>
           <div class="form-group" align="center">
              <button class="btn btn-primary" type="submit" name="submit">Registrar Datos</button>
           </div> 
        <?php echo form_close();?>
      </div> 
  
  </body>
</html>