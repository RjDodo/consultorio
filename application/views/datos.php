<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Mostrar datos</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>assets/css/navbar.css" rel="stylesheet">
</head>
<body>
	
   <div class="container">
	      <div class="row">
	          <div class="col-md-6">  

	                  <ul class="nav nav-pills">
	                    <li class="nav-item">
	                      <a class="nav-link active" href="<?php echo base_url(); ?>index.php/inicio/registro">Inicio</a>
	                    </li>
	                    <li class="nav-item">
	                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/ver_historial_todos">Mostrar Historias Médicas</a>
	                    </li>
	                  </ul>                  
	          </div>
	          <div class="col-md-6">  
	              <img src="<?php echo base_url(); ?>img/img1_consulta.jpg" width="83%" alt="">
	          </div>
	     </div>
    </div>
	<!--Primera fila de navegacion e imagen -->
	
      
    <div class="row" align="center">
    	<div class="col-md-12">
    		<div class="form-group" >
				<?php echo form_open("inicio/buscar"); ?>
					<input type="text" name="email" id="email">
					<input type="submit" name="buscar" id="buscar" value="Buscar" class="btn btn-success">
				<?php echo form_close(); ?>
		   </div>
    	</div>
    </div>

	<h1 align="center">Registro de base de datos </h1>
	<div class="container">
		<div class="row">
			<div class="col-md-12" >
				<br/>
				<br/>
				<div class="container">
					
					<table class ="table table-striped table-hover table table-sm"  >
					<thead = class ="table-primary">
						<tr >
							<th scope = 'col'>Id_usuario</th>
							<th scope = 'col'>Nombre</th>
							<th scope = 'col'>Apellido</th>
							<th scope = 'col'>Correo</th>
							<th scope = 'col'>Edad</th>
							<th scope = 'col'>Sexo</th>
							<th scope = 'col'>Direcciòn</th>
							<th scope = 'col'>Tipo de Sangre</th>
							<th scope = 'col'>Alergias</th>
							<th scope = 'col'>Fecha de registro</th>
							<th scope = 'col'>Fecha de modificaciòn</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $user){?>
						 <tr>
						 	<td><?php echo $user->id_usuario; ?></td>
						 	<td><?php echo $user->first_name; ?></td>
						 	<td><?php echo $user->last_name; ?></td>
						 	<td><?php echo $user->email; ?></td>
						 	<td><?php echo $user->age; ?></td>
						 	<td><?php echo $user->sex; ?></td>
						 	<td><?php echo $user->address; ?></td>
						 	<td><?php echo $user->blood_type; ?></td>
						 	<td align="center" width="200" height="200" ><?php echo $user->allergies; ?></td>
						 	<td><?php echo $user->registration_date; ?></td>
						 	<td><?php echo $user->modification_date; ?></td>
						 	<td></td>
						 	<td><a href=" <?php echo base_url()?>index.php/inicio/ver_historial/<?=$user->id_usuario ?>">Ver</a> | <a href=" <?php echo base_url()?>index.php/inicio/editar/<?=$user->id_usuario ?>">Editar</a>  |
						 	<a href=" <?php echo base_url()?>index.php/inicio/eliminar/<?=$user->id_usuario ?>">Eliminar</a></td>
						 </tr>
						<?php } ?>
					</tbody>
				</table>

				</div>
				
			</div>
		</div>
	</div>
</body>
</html>