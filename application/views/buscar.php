<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Buscar datos</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
      <div class="row">
          <div class="col-md-6" style="color:#fff">  

                  <ul class="nav nav-pills">
                    <li class="nav-item">
                      <a class="nav-link active " href="<?php echo base_url(); ?>index.php/inicio/registro">Inicio</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar Datos</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/ver_historial_todos">Mostrar Historias Médicas</a>
                    </li>
                  </ul>                  
          </div>
          <div class="col-md-6">  
              <img src="<?php echo base_url(); ?>img/img1_consulta.jpg" width="83%" alt="">
          </div>
     </div>
</div>
<br><br><br>

	<h1 align="center">Buscar datos </h1>
	<div class="container">
		<div class="row">
			<div class="col" align="center">
				<?php echo form_open("inicio/buscar"); ?>
					<input type="text" name="email" id="email">
					<input type="submit" name="buscar" id="buscar" value="Buscar">
				<?php echo form_close(); ?>
				<br/>
				<br/>
				<table class="table table-bordered" align="center" border="3px">
					<thead>
						<tr>
							<th>Id_usuario</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Edad</th>
							<th>Sexo</th>
							<th>Direcciòn</th>
							<th>Tipo de Sangre</th>
							<th>Alergias</th>
							<th>Fecha de registro</th>
							<th>Fecha de modificaciòn</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($query as $user){?>
						 <tr>
						 	<td><?php echo $user->id_usuario; ?></td>
						 	<td><?php echo $user->first_name; ?></td>
						 	<td><?php echo $user->last_name; ?></td>
						 	<td><?php echo $user->email; ?></td>
						 	<td><?php echo $user->age; ?></td>
						 	<td><?php echo $user->sex; ?></td>
						 	<td><?php echo $user->address; ?></td>
						 	<td><?php echo $user->blood_type; ?></td>
						 	<td align="center" width="100" height="100" ><?php echo $user->allergies; ?></td>
						 	<td><?php echo $user->registration_date; ?></td>
						 	<td><?php echo $user->modification_date; ?></td>
						 	<td></td>
						 	<td><a href=" <?php echo base_url()?>index.php/inicio/ver_historial/<?=$user->id_usuario ?>">Ver</a> | <a href=" <?php echo base_url()?>index.php/inicio/editar/<?=$user->id_usuario ?>">Editar</a>  |
						 	<a href=" <?php echo base_url()?>index.php/inicio/eliminar/<?=$user->id_usuario ?>">Nueva historia</a></td>
						 </tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>