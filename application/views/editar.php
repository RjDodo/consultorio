<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Editar</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
      <div class="row">
          <div class="col-md-6" style="color:#fff">  

                  <ul class="nav nav-pills">
                    <li class="nav-item">
                      <a class="nav-link active " href="<?php echo base_url(); ?>index.php/inicio/registro">Inicio</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar Datos</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/ver_historial_todos">Mostrar Historias Médicas</a>
                    </li>
                  </ul>                  
          </div>
          <div class="col-md-6">  
              <img src="<?php echo base_url(); ?>img/img1_consulta.jpg" width="83%" alt="">
          </div>
     </div>
</div>
<br><br><br>

	<h1 align="center">Editar registro</h1><br><br>
	<div align="center">
		<?php echo validation_errors();  ?>
		<?php echo form_open("inicio/editarRegistro/$id_usuario") ?>
	      <input type="text" name="first_name" placeholder="Nombre" id="first_name"  value= "<?=$first_name; ?>"><br><br>
	      <input type="text" name="last_name" placeholder="Apellido" value="<?=$last_name?>"><br><br>
	      <input type="text" name="email" placeholder="Correo" value="<?=$email;?>"><br><br>
	      <input type="text" name="age" placeholder="Edad" value="<?=$age; ?>"><br><br>
	      <label for="">Sexo</label>
          <select name="sex" id="sex">
            <option value="M">Masculino</option>
            <option value="F">Femenino</option>
          </select><br><br>

          <textarea name="address" id="address" cols="30" rows="10" placeholder="Direcciòn de domicilio"><?php echo $address; ?></textarea><br><br>
          <label for="">Tipo de sangre</label>
          <select name="blood_type" >
            <option value="O POSITIVO">O POSITIVO</option>
            <option value="O NEGATIVO">O NEGATIVO</option>
            <option value="A POSITIVO">A POSITIVO</option>
            <option value="A NEGATIVO">A NEGATIVO</option>
            <option value="B POSITIVO">B POSITIVO</option>
            <option value="B NEGATIVO">B NEGATIVO</option>
            <option value="AB POSITIVO">AB POSITIVO</option>
            <option value="AB NEGATIVO">AB NEGATIVO</option>
          </select><br> <br>
         <textarea name="allergies"   id="allergies" cols="100" rows="10" placeholder="Llenar si posee algùn cuadro alergico"><?php echo $allergies; ?></textarea><br><br>
          <label for="">Fecha registro:</label>
          <input type="date" name="registration_date" placeholder="fecha de registro" value="<?php echo set_value('registration_date')?>"><br><br>
          <label for="">Fecha modificacion:</label>
          <input type="date" name="modification_date" placeholder="fecha de registro" value="<?php echo set_value('modification_date')?>"><br><br>
	      <button type="submit" name="editar" id="editar">Guardar datos</button>
    	 <?php echo form_close();?>	
	</div>
	

</body>
</html>