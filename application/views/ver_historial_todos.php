<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Mostrar datos</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

</head>
<body>
<br>
<div class="container">
      <div class="row">
          <div class="col-md-6" style="color:#fff">  

                  <ul class="nav nav-pills">
                    <li class="nav-item">
                      <a class="nav-link active " href="<?php echo base_url(); ?>index.php/inicio/registro">Inicio</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link " href="<?php echo base_url(); ?>index.php/inicio/datos">Mostrar Datos</a>
                    </li>
                  </ul>                  
          </div>
          <div class="col-md-6">  
              <img src="<?php echo base_url(); ?>img/img1_consulta.jpg" width="83%" alt="">
          </div>
     </div>
</div>
<br><br><br>

	<h1 align="center">HISTORIAL MÉDICO </h1>
	<div class="container">
		<div class="row">
			<div class="col-md-12" align="center">
				<?php echo form_open("inicio/buscar"); ?>
					<input type="text" name="email" id="email">
					<input type="submit" name="buscar" id="buscar" value="Buscar">
				<?php echo form_close(); ?>
				<br/>
				<br/>
			</div>
				
        </div>
        <div class="row">
        	<div class="col-md-12" align="center">
        		<table  class ="table table-striped table-hover table table-sm">
					<thead = class ="table-primary">
						<tr>
							<th>Id</th>
							<th>id_usuario</th>
							<th>sintomas</th>
							<th>Tipo de seguro</th>
							<th>Fecha de registro</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($historial as $user){?>
						 <tr>
						 	<td><?php echo $user->id_historial;?></td>
						 	<td><?php echo $user->id_usuario; ?></td>
						 	<td><?php echo $user->sintomas; ?></td>
						 	<td><?php echo $user->tipo_seguro; ?></td>
						 	<td><?php echo $user->fecha_ingreso; ?></td>
						 	<td></td>
						 </tr>
						<?php } ?>
					</tbody>
				</table>
        	</div>
        </div>
				
			
		
	</div>
</body>
</html>