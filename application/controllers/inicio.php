<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct(){

		parent::__construct();
		$this->load->model('datos_model');
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->view('principal');
	}
	// realizando prueba de actualizacion de linea en git


	public function datos($id_usuario=null)
	{
		if(isset($this->session->userdata['welcome']))
		{
			//vamos a cargar el modelo
		//realizamos un arreglo con los datos de la tabla users y las funciones  que recibimos del modelo
		$data = array (
			'users'=>$this->datos_model->obtenerTodos(),
			'userForId'=>$this->datos_model->obtenerPorId($id_usuario),
			'total' =>$this->datos_model->totalRegistros()
		);
		//
		
		$this->load->view('datos',$data);
		//$this->load->view('datos_por_id',$data);	
		}
		else
		{
			 redirect('inicio/login');	
		}		
		
	}
	public function registro()
	{
		$this->load->view('registro');
	}

	/**

	Comentario:_______________________________________________________
	Tipos de datos que recibe la funcion: Nombre, Apellido, Email, Edad.

	Autor: Ricardo Donado

	*/

	public function save()
	{
		$this->form_validation->set_rules('first_name','Nombre','required|alpha', array('required' => 'El campo nombre es requerido.', 'alpha' => 'El nombre solo debe tener letras.' ));
		$this->form_validation->set_rules('last_name','Apellido','required|alpha', array('required' => 'El campo apellido es requerido.', 'alpha' => 'El apellido solo debe tener letras.'));
		$this->form_validation->set_rules('email','Email','valid_email|required');
		$this->form_validation->set_rules('age','Edad','is_numeric|required',array('required' => 'El campo edad es requerido.', 'is_numeric' => 'La Edad solo debe tener numeros enteros positivos.' ));

		if($this->form_validation->run()==TRUE){

			$email = $this->input->post('email');

			if($this->datos_model->validar_email($email)== FALSE){
				 $data= array(
				'first_name'=>$this->input->post('first_name'),
				'last_name'=>$this->input->post('last_name'),
				'email'=>$this->input->post('email'),
				'age'=>$this->input->post('age'),
				'sex'=>$this->input->post('sex'),
				'address'=>$this->input->post('address'),
				'blood_type'=>$this->input->post('blood_type'),
				'allergies'=>$this->input->post('allergies'),
				'registration_date'=>$this->input->post('registration_date'),
				'modification_date'=>$this->input->post('modification_date')
				);
				$id_usuario = $this->datos_model->user_save($data);
				$data = array('id_usuario' => $id_usuario); 
				$this->load->view('historial',$data);

			}
			else{
				$data['message'] = "El correo electronico ya existe.";
				$this->load->view('registro', $data);
			}
				
	 	}else
	 	{
			$this->load->view('registro');
	 	}
	}


	// Función para ver los datos persoanles del usuario.____________________________

	public function verUser($id_usuario=null)
	{
		
		$data = array(
			'userForId'=> $this->datos_model->obtenerPorId($id_usuario),
			'total'=> $this->datos_model->totalRegistros());
		
		$this->load->view('datos_por_id', $data);
	}

	// Función para editar algun cambio de dato del usuario.___________________________

	public function editar($id_usuario = null)
	{
		 
		if($id_usuario == null){
			redirect('inicio/datos');
		}

	// VALIDAR QUE EL ID SEA UN NUMERO.________________________________________________

		$obtenerdatos = $this->datos_model->obtenerdatos($id_usuario);
		if ($obtenerdatos == TRUE){
			foreach ($obtenerdatos->result() as $row)
			{
				$first_name = $row->first_name;
				$last_name = $row->last_name;
				$email = $row->email;
				$age = $row->age;
				$sex = $row->sex;
				$address = $row->address;
				$blood_type = $row->blood_type;
				$allergies = $row->allergies;


			}

			$data = array(
				'id_usuario' => $id_usuario, 
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'age' => $age,
				'sex' => $sex,
				'address' => $address,
				'blood_type' => $blood_type,
				'allergies' => $allergies);
		}else{
			$data = '';
		}
		$this->load->view('editar', $data);

	}

	public function editarRegistro($id_usuario=null)

	{
		$this->form_validation->set_rules('first_name','Nombre','required|alpha', array('required' => 'El campo nombre es requerido.', 'alpha' => 'El nombre solo debe tener letras.' ));
		$this->form_validation->set_rules('last_name','Apellido','required|alpha', array('required' => 'El campo apellido es requerido.', 'alpha' => 'El apellido solo debe tener letras.'));
		$this->form_validation->set_rules('email','Email','valid_email|required');
		$this->form_validation->set_rules('age','Edad','is_numeric|required',array('required' => 'El campo edad es requerido.', 'is_numeric' => 'La Edad solo debe tener numeros enteros positivos.' ));

		if($this->form_validation->run()==TRUE)

		{
			$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'age' => $this->input->post('age'),
			'sex'=>$this->input->post('sex'),
			'address'=>$this->input->post('address'),
			'blood_type'=>$this->input->post('blood_type'),
			'allergies'=>$this->input->post('allergies'),
			'registration_date'=>$this->input->post('registration_date'),
			'modification_date'=>$this->input->post('modification_date')
		);
	
		$this->datos_model->editarRegistro($id_usuario, $data);
		$this->load->view('datos_guardados');
		}else{
			redirect('inicio/datos');
		}

	}

	public function eliminar($id_usuario=null)

	{
		if($id_usuario == null){
			redirect('inicio/datos');
		}

		$data = array('id_usuario' => $id_usuario);
		$this->datos_model->eliminar($id_usuario);
		redirect('inicio/datos');

	}

	public function llenar_historia()
	{

		$this->load->view('historial');
	}

	public function llenar_nueva_historia($id_usuario=null)
	{
		$this->load->view('historial');
	}

	public function guardar_nueva_historia($id_usuario=null)
	{
		
		$this->form_validation->set_rules('sintomas','Sintomas','required', array('required' => 'El campo Sintomas es requerido.'));
		if ($this->form_validation->run()==TRUE) {

			$data = array(
			'id_usuario' => $this->input->post('id_usuario'),	
			'sintomas' => $this->input->post('sintomas'),
			'seguro' => $this->input->post('seguro'),
			'tipo_seguro' => $this->input->post('tipo_seguro'),
			'fecha_ingreso' => $this->input->post('fecha_ingreso')
		);
			
			$this->datos_model->guardar_nueva_historia($data);
			redirect ('inicio/exito');
			
		
		}else{
			echo ' NO SE GUARDO HISTORIA MEDICA';
			
		}

	}

	public function guardar_historia()
	{
		
		$this->form_validation->set_rules('sintomas','Sintomas','required', array('required' => 'El campo Sintomas es requerido.'));
		if ($this->form_validation->run()==TRUE) {

			$data = array(
			'id_usuario' => $this->input->post('id_usuario'),	
			'sintomas' => $this->input->post('sintomas'),
			'seguro' => $this->input->post('seguro'),
			'tipo_seguro' => $this->input->post('tipo_seguro'),
			'fecha_ingreso' => $this->input->post('fecha_ingreso')
		);
			
			$this->datos_model->guardar_historia($data);
			redirect ('inicio/exito');
			
		
		}else{
			echo ' NO SE GUARDO HISTORIA MEDICA';
			
		}

	}

	public function buscar()
	{
		$email = $this->input->post('email');
		$data['query'] = $this->datos_model->buscar($email);
		$this->load->view('buscar',$data);
	}

	public function ver_historial($id_usuario=null)
	{
		//$data['historial'] = $this->datos_model->ver_historial($id_usuario);
		$data['usuario'] = $this->datos_model->obtenerPorId($id_usuario);
		$this->load->view('ver_historial', $data);
	}

	public function exito()
	{
		$this->load->view('exitosamente');
	}
	public function ver_historial_todos()
	{
		$data['historial'] = $this->datos_model->ver_historial_todos();
		$this->load->view('ver_historial_todos', $data);
	}


	public function joins($id_usuario = ''){
  	$data['usuario'] = $this->datos_model->obtenerPorId($id_usuario);
  	return $data;

  }

  	public function personal()
  	{
  		$this->load->view('personal');
  	}

  	public function registrar_personal()
  	{
  		$this->form_validation->set_rules('first_name','Nombre','required|alpha', array('required' => 'El campo nombre es requerido.', 'alpha' => 'El nombre solo debe tener letras.' ));
		$this->form_validation->set_rules('last_name','Apellido','required|alpha', array('required' => 'El campo apellido es requerido.', 'alpha' => 'El apellido solo debe tener letras.'));
		$this->form_validation->set_rules('email','Email','valid_email|required');
		//$this->form_validation->set_rules('password','Password','valid_password|required');
		//$this->form_validation->set_rules('conf_password','Password','valid_password|required');*

		if($this->form_validation->run()==TRUE){

			$email = $this->input->post('email');

			if($this->datos_model->validar_email_personal($email)== FALSE){
				 $data= array(
				'first_name'=>$this->input->post('first_name'),
				'last_name'=>$this->input->post('last_name'),
				'email'=>$this->input->post('email'),
				'password'=>md5($this->input->post('password')),
				'conf_password'=>md5($this->input->post('conf_password')),
				'address'=>$this->input->post('address'));
				
				$this->datos_model->save_personal($data); 
				$this->load->view('login',$data);

			}
			else{
				$data['message'] = "El correo electronico ya existe.";
				$this->load->view('personal', $data);
			}
				
	 	}else
	 	{
			$this->load->view('login');
	 	}
	}

	public function login()
	{
		// validacion de los campos de entrada del formulario.__________________________
		$this->form_validation->set_rules('first_name', 'first_name', 'required|alpha');
        $this->form_validation->set_rules('password', 'password', 'required');
        ///echo 'fuera de las validaciones';
         if ($this->form_validation->run() == TRUE)
         {
            $data= array(
            		'first_name' => $this->input->post('first_name'),
            		'password'=>md5($this->input->post('password')));

            $result = $this->datos_model->login($data);
            
            if ($result != FALSE) {
            	

            	$data=array(
            		'id' => $result->id_personal,
            		'first_name' => $result->first_name,
            		'logueado' => TRUE
            	);
            	$this->session->set_userdata('welcome', $data);
            	
            	redirect('inicio/logueado');
            	
            }else{
            	
            	redirect('inicio/login');
            }
        }
	// REVISAR ion auth codeigniter!!!!!

     }

     public function logueado()
     {
      if(isset($this->session->userdata['welcome'])){
         $data = array();

         $id =  $this->session->userdata['welcome']['id'];

         $data['first_name'] = $this->session->userdata['welcome']['first_name'];
         $this->load->view('registro', $data);
      }else{
         redirect('inicio/login');
      }
      
     }

     public function cerrar_sesion() {
      $usuario_data = array(
         'logueado' => FALSE
      );
      $this->session->set_userdata('welcome',$usuario_data);
      redirect('inicio/login');
   }
}    