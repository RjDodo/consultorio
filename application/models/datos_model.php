<?php 
class Datos_model extends CI_Model
{
	
	public function obtenerTodos()
	{
		$this->db->select('*');
		$this->db->from('users');
		$query = $this->db->get();
		return $query->result();
	}

	
	public function totalRegistros()
	{
		return $this->db->count_all_results("users");
	}


	//guardar datos en base de datos
	public function user_save($data){
	 	
		$this->db->insert('users',$data);
		$id_usuario = $this->db->insert_id('users');
		if($id_usuario > 0)
			return $id_usuario;
		else
			return FALSE;
	}


	public function update($id_usuario,$first_name,$last_name,$email,$age,$sex,$address,$blood_type,$allergies,$registration_date,$modification_date)
	{
		$this->db->where('id_usuario',$id_usuario)	;
		$this->db->set('first_name',$first_name);
		return $this->db->update('users')	;

	}

	public function obtenerdatos($id_usuario)
	{
		//$this->db->select('*');
		//$this->db->from('users');
		$this->db->where('id_usuario',$id_usuario);
		$query = $this->db->get('users');
		if ($query->num_rows() == 1)
		{
			return $query;
		}else{
			return FALSE;
		}
	}
	
	public function editarRegistro($id_usuario, $data)
	{
		$this->db->where('id_usuario',$id_usuario);
		$this->db->update('users', $data);
	}
	// Borrar usuario e historial 

	public function eliminar($id_usuario)
	{
		$this->db->where('id_usuario', $id_usuario); 
		$this->db->delete('users');
		$this->db->where('id_usuario', $id_usuario);
		$this->db->delete('historial');

	}


	/* Validar correo ùnico */

	public function validar_email($email)
	{
		$this->db->select('id_usuario');
		$this->db->where('email', $email);
		$query = $this->db->get('users');
		if ($query->num_rows() == 1)
		{
			return TRUE;
		}
		return FALSE;		
	}

	public function guardar_historia($data)
	{
		if($this->db->insert('historial',$data))
		{
			return TRUE;
		}
	}

	public function buscar($email)
	{
		$this->db->select('*');
		$this->db->like('email', $email);
		$query = $this->db->get('users');
		return $query->result();
	}




	public function obtenerPorId($id_usuario)
	{
		/* SELECT users.first_name, users.last_name, historial.sintomas FROM `users` LEFT JOIN historial on users.id_usuario = historial.id_usuario WHERE users.id_usuario = 1 */

		// UTilizando la funcion JOIN para realizar una sola consulta

		$this->db->select('users.first_name, users.last_name, users.email, users.age,
			               users.sex, users.address, users.blood_type, users.allergies, users.registration_date, users.modification_date, historial.id_historial, historial.id_usuario, historial.sintomas, historial.seguro, historial.tipo_seguro, historial.fecha_ingreso');
		$this->db->from('users');
		$this->db->join('historial', 'users.id_usuario = historial.id_usuario', 'LEFT');
		$this->db->where('users.id_usuario',$id_usuario);
		$query = $this->db->get();
		return $query->result();


	}

	public function ver_historial_todos()
	{
		$this->db->select('*');
		$this->db->from('historial');
		$query = $this->db->get();
		return $query->result();
	}

	public function guardar_nueva_historia($id_usuario, $data)
	{
		$this->db->where('id_usuario',$id_usuario);
		$this->db->update('historial', $data);
	}

	public function save_personal($data)
	{
		$this->db->insert('personal',$data);
	}

	public function validar_email_personal($email)
	{
		$this->db->select('id_personal');
		$this->db->where('email', $email);
		$query = $this->db->get('personal');
		if ($query->num_rows() == 1)
		{
			return TRUE;
		}
		return FALSE;		
	}

	//-----------------------------------SESION------------------------------
   public function login($otravariable)
   {
	   $this ->db->select('id_personal, first_name, password');
	   $this ->db->from('personal');
	   $this ->db->where('first_name', $otravariable['first_name']);
	   $this ->db->where('password', $otravariable['password']);
	   //$this ->db->limit(1);
	   $query = $this ->db-> get();
	   if($query -> num_rows() == 1){
	     return $query->row();
	   }else{
	     return false;
	   }
	}

	//Leer los datos para devolverlas en variable de sesion
	public function read_personal_information($first_name) 
	{
	    $this->db->select('*');
	    $this->db->from('personal');
	    $this->db->where('first_name', $first_name);
	    $this->db->limit(1);
	    $query = $this->db->get();

	    if ($query->num_rows() == 1) {
	     return $query->result();
	    } else {
	       return false;
	    }
	}

	 


}//Fin de la clase modelos datos_model



 